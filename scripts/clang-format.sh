#!/bin/sh
for file in `ls src/*.h* src/*.c*`
do
    clang-format -style=file $file | \
    diff --color=always -u $file - | \
    sed -e "1s|--- |--- a/|" -e "2s|+++ -|+++ b/$file|"
done
