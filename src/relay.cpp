#include "relay.hpp"
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

using namespace std;

namespace Brewserver {
	Relay::Relay(int gpioPin, bool activeLow) :
		m_gpioPin(gpioPin),
		m_activeLow(activeLow)
	{
		this->m_sysPath = string("/sys/class/gpio/gpio") + to_string(gpioPin) + string("/");

		ofstream ex("/sys/class/gpio/export");
		ex << this->m_gpioPin;
		ex.close();

		// give the system a chance to setup the gpio sysfs
		this_thread::sleep_for(chrono::milliseconds(50));

		ofstream dir(string(this->m_sysPath + "direction").c_str());
		// outputing high will set the mode to output and set the value
		// to high, so that the relays aren't turned on immediately
		if (activeLow)
			dir << "high";
		else
			dir << "out";
		dir.close();

		// give the pin a chance to change modes
		this_thread::sleep_for(chrono::milliseconds(50));

		int alVal = activeLow ? 1 : 0;
		ofstream al(this->m_sysPath + "active_low");
		al << alVal;
		al.close();

		this->Off();
	}

	Relay::~Relay()
	{
		this->Off();

		ofstream al(this->m_sysPath + "active_low");
		al << 0 << "\n";
		al.close();

		ofstream dir(this->m_sysPath + "direction");
		dir << "in";
		dir.close();

		ofstream uex("/sys/class/gpio/unexport");
		uex << this->m_gpioPin;
		uex.close();
	}

	Relay::RelayState Relay::State()
	{
		ifstream valF(this->m_sysPath + "value");
		int val;
		valF >> val;
		if (val == 1) return Relay::RelayState::ON;
		return Relay::RelayState::OFF;
	}

	void Relay::On()
	{
		ofstream val(this->m_sysPath + "value");
		val << 1;
	}

	void Relay::Off()
	{
		ofstream val(this->m_sysPath + "value");
		val << 0;
	}
} // namespace Brewserver
