#include "thermostat.hpp"
#include <chrono>
#include <cstdio>
#include <cstring>
#include <ctime>

using namespace std;

namespace Brewserver {
	string ThermostatData::TargetSensor()
	{
		string ret;
		this->Acquire();
		ret = string(this->m_data->TargetSensor);
		this->Release();
		return ret;
	}

	void ThermostatData::TargetSensor(string value)
	{
		this->Acquire();
		char *v = this->m_data->TargetSensor;
		memset(v, 0, 26);
		memcpy(v, value.c_str(), value.length());
		this->Release();
	}

	string ThermostatData::LimitSensor()
	{
		string ret;
		this->Acquire();
		ret = string(this->m_data->LimitSensor);
		this->Release();
		return ret;
	}

	void ThermostatData::LimitSensor(string value)
	{
		this->Acquire();
		char *v = this->m_data->LimitSensor;
		memset(v, 0, 26);
		memcpy(v, value.c_str(), value.length());
		this->Release();
	}

	float ThermostatData::TargetTemp()
	{
		float ret;
		this->Acquire();
		ret = this->m_data->TargetTemp;
		this->Release();
		return ret;
	}

	void ThermostatData::TargetTemp(float value)
	{
		this->Acquire();
		this->m_data->TargetTemp = value;
		this->Release();
	}

	float ThermostatData::LimitTemp()
	{
		float ret;
		this->Acquire();
		ret = this->m_data->LimitTemp;
		this->Release();
		return ret;
	}

	float ThermostatData::Threshold()
	{
		float ret;
		this->Acquire();
		ret = this->m_data->Threshold;
		this->Release();
		return ret;
	}

	void ThermostatData::Threshold(float value)
	{
		this->Acquire();
		this->m_data->Threshold = value;
		this->Release();
	}

	void ThermostatData::LimitTemp(float value)
	{
		this->Acquire();
		this->m_data->LimitTemp = value;
		this->Release();
	}

	time_t ThermostatData::LastUpdated()
	{
		time_t ret;
		this->Acquire();
		ret = this->m_data->LastUpdated;
		this->Release();
		return ret;
	}

	void ThermostatData::LastUpdated(time_t value)
	{
		this->Acquire();
		this->m_data->LastUpdated = value;
		this->Release();
	}

	Thermostat::ThermostatStatus ThermostatData::Status()
	{
		Thermostat::ThermostatStatus ret;
		this->Acquire();
		ret = this->m_data->Status;
		this->Release();
		return ret;
	}

	void ThermostatData::Status(Thermostat::ThermostatStatus value)
	{
		this->Acquire();
		this->m_data->Status = value;
		this->Release();
	}

	Thermostat::ThermostatMode ThermostatData::Mode()
	{
		Thermostat::ThermostatMode ret;
		this->Acquire();
		ret = this->m_data->Mode;
		this->Release();
		return ret;
	}

	void ThermostatData::Mode(Thermostat::ThermostatMode value)
	{
		this->Acquire();
		this->m_data->Mode = value;
		this->Release();
	}

	string ThermostatData::DataPath(string name)
	{
		return string("/brewserver_therm_") + name + string("_data");
	}

	string ThermostatData::SemPath(string name)
	{
		return string("/brewserver_therm_") + name;
	}

	Thermostat::Thermostat(
		string name,
		string targetSensor,
		string limitSensor,
		ThermostatMode mode,
		float targetTemp,
		float limitTemp,
		float threshold,
		int gpioPin)
	{
		this->oldMode = mode;
		this->m_running = false;
		this->m_name = name;
		this->m_targetSensorName = targetSensor;
		this->m_limitSensorName = limitSensor;
		this->m_gpioPin = gpioPin;
		this->m_data = new ThermostatData(this->m_name, true);
		this->m_data->TargetSensor(targetSensor);
		this->m_data->LimitSensor(limitSensor);
		this->m_data->TargetTemp(targetTemp);
		this->m_data->LimitTemp(limitTemp);
		this->m_data->Threshold(threshold);
		this->m_data->Status(ThermostatStatus::OFF);
		this->m_data->Mode(mode);

		this->m_targetSensor = new TemperatureSensorData(this->m_targetSensorName);
		this->m_limitSensor = new TemperatureSensorData(this->m_limitSensorName);

		this->m_relay = new Relay(this->m_gpioPin, true);
	}

	Thermostat::~Thermostat()
	{
		if (this->m_running) this->Stop();
		delete this->m_data;
		delete this->m_relay;
	}

	void Thermostat::DoCoolMode()
	{
		float curTargetTemp = this->m_targetSensor->TempF();
		float curLimitTemp = this->m_limitSensor->TempF();

		switch (this->m_relay->State()) {
		case Relay::RelayState::ON: // low / on
			if (curTargetTemp < this->m_data->TargetTemp() ||
				curLimitTemp < this->m_data->LimitTemp()) {
				// turn off
				printf("%s COOL On -> Off\n", this->m_name.c_str());
				this->m_relay->Off();
				if (curLimitTemp < this->m_data->LimitTemp())
					this->m_data->Status(ThermostatStatus::WAIT);
				else
					this->m_data->Status(ThermostatStatus::OFF);
			}
			break;
		case Relay::RelayState::OFF: // high / off
			if (curTargetTemp > (this->m_data->TargetTemp() + this->m_data->Threshold()) &&
				curLimitTemp > (this->m_data->LimitTemp() + this->m_data->Threshold())) {
				// turn on
				printf("%s COOL Off -> On\n", this->m_name.c_str());
				this->m_relay->On();
				this->m_data->Status(ThermostatStatus::ON);
			}
		}
	}

	void Thermostat::DoHeatMode()
	{
		float curTargetTemp = this->m_targetSensor->TempF();
		float curLimitTemp = this->m_limitSensor->TempF();
		switch (this->m_relay->State()) {
		case Relay::RelayState::ON: // low / on
			if (curTargetTemp > this->m_data->TargetTemp() ||
				curLimitTemp > this->m_data->LimitTemp()) {
				// turn off
				printf("%s HEAT On -> Off\n", this->m_name.c_str());
				this->m_relay->Off();
				if (curLimitTemp > this->m_data->LimitTemp())
					this->m_data->Status(ThermostatStatus::WAIT);
				else
					this->m_data->Status(ThermostatStatus::OFF);
			}
			break;
		case Relay::RelayState::OFF: // high / off
			if (curTargetTemp < (this->m_data->TargetTemp() - this->m_data->Threshold()) &&
				curLimitTemp < (this->m_data->LimitTemp() - this->m_data->Threshold())) {
				// turn on
				printf("%s HEAT Off -> On\n", this->m_name.c_str());
				this->m_relay->On();
				this->m_data->Status(ThermostatStatus::ON);
			}
		}
	}

	void Thermostat::Run(Thermostat *therm)
	{
		while (therm->m_running) {
			switch (therm->m_data->Mode()) {
			case ThermostatMode::OFF:
				therm->m_data->Status(ThermostatStatus::OFF);
				// turn off GPIO if it's not
				if (therm->m_relay->State() == Relay::RelayState::ON) therm->m_relay->Off();
				break;
			case ThermostatMode::COOL:
				therm->DoCoolMode();
				break;
			case ThermostatMode::HEAT:
				therm->DoHeatMode();
				break;
			}
			therm->m_data->LastUpdated(time(NULL));
			this_thread::sleep_for(chrono::milliseconds(1000));
		}
	}

	bool Thermostat::Start()
	{
		this->m_running = true;
		this->m_thread = new thread(Thermostat::Run, this);
		return true;
	}

	bool Thermostat::Stop()
	{
		if (!this->m_thread || !this->m_running) {
			return false;
		}

		this->m_running = false;
		this->m_thread->join();
		return true;
	}
} // namespace Brewserver
