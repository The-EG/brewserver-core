#pragma once
#include <string>

namespace Brewserver {
	class Relay {
	public:
		enum class RelayState {
			OFF,
			ON
		};

	private:
		int m_gpioPin;
		bool m_activeLow;
		std::string m_sysPath;

	public:
		Relay(int gpioPin, bool activeLow = false);
		~Relay();

		RelayState State();
		void On();
		void Off();
	};
} // namespace Brewserver
