#include "sighandler.hpp"
#include <signal.h>
#include <cstdio>

namespace Brewserver {
	bool SignalHandler::s_SIGINT = false;
	bool SignalHandler::s_SIGTERM = false;

	void SignalHandler::OnSignal(int signal)
	{
		switch (signal) {
		case SIGINT: // keyboard interrupt, etc.
			SignalHandler::s_SIGINT = true;
			break;
		case SIGTERM:
			SignalHandler::s_SIGTERM = true;
			break;
		default:
			fprintf(stderr, "SignalHandler::OnSignal not implemented for %d\n", signal);
			break;
		}
	}

	void SignalHandler::SetHandleSignal(int signal, bool handle)
	{
		struct sigaction sa;
		if (handle)
			sa.sa_handler = SignalHandler::OnSignal;
		else
			sa.sa_handler = SIG_DFL;

		sa.sa_flags = NULL;
		//sa.sa_mask = NULL;
		sigaction(signal, &sa, NULL);
	}
} // namespace Brewserver
