#pragma once

namespace Brewserver {
	class SignalHandler {
	private:
		SignalHandler() {} // static only

		static void OnSignal(int signal);

		static bool s_SIGINT;
		static bool s_SIGTERM;

	public:
		/* don't allow instantiating or assignment */
		SignalHandler(SignalHandler const &) = delete;
		void operator=(SignalHandler const &) = delete;

		static void SetHandleSignal(int signal, bool handle = true);

		static bool HasSIGINT() { return SignalHandler::s_SIGINT; }
		static bool HasSIGTERM() { return SignalHandler::s_SIGTERM; }

		static void ResetSIGINT() { SignalHandler::s_SIGINT = false; }
		static void ResetSIGTERM() { SignalHandler::s_SIGTERM = false; }
	};
} // namespace Brewserver
