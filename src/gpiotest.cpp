#include "gpio.hpp"
#include <chrono>
#include <cstdio>
#include <thread>

using namespace Brewserver;

int main(int argc, char *argv[])
{
	GPIOController *gpio = new GPIOController();
	printf("GPIO Pin 23 Mode = %d\n", gpio->PinMode(23));
	gpio->PinMode(23, GPIOController::PinModeValue::OUTPUT);
	printf("GPIO Pin 23 Mode = %d\n", gpio->PinMode(23));

	printf("GPIO Pin 23 Level = %d\n", gpio->PinLevel(23));
	gpio->SetPin(23);
	printf("GPIO Pin 23 Level = %d\n", gpio->PinLevel(23));
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	gpio->ClearPin(23);
	printf("GPIO Pin 23 Level = %d\n", gpio->PinLevel(23));
	gpio->PinMode(23, GPIOController::PinModeValue::INPUT);
	delete gpio;
	return 0;
}
