#pragma once
#include "mem.hpp"
#include "relay.hpp"
#include "sensor.hpp"
#include <cstdint>
#include <ctime>
#include <string>
#include <thread>

namespace Brewserver {
	class ThermostatData;

	class Thermostat {
	public:
		enum class ThermostatStatus : uint16_t {
			OFF = 0,
			ON = 1,
			WAIT = 2
		};

		enum class ThermostatMode : uint16_t {
			OFF = 0,
			COOL = 1,
			HEAT = 2
		};

		Thermostat(
			std::string name,
			std::string targetSensor,
			std::string limitSensor,
			ThermostatMode mode,
			float targetTemp,
			float limitTemp,
			float threshold,
			int gpioPin);
		~Thermostat();

		bool Start();
		bool Stop();

	private:
		bool m_running;
		std::string m_name;
		std::string m_targetSensorName;
		std::string m_limitSensorName;
		std::thread *m_thread;
		ThermostatData *m_data;
		TemperatureSensorData *m_targetSensor;
		TemperatureSensorData *m_limitSensor;
		static void Run(Thermostat *therm);
		void DoCoolMode();
		void DoHeatMode();
		ThermostatMode oldMode;
		//GPIOController *m_gpio;
		Relay *m_relay;
		int m_gpioPin;
	};

	class ThermostatData : public RWSharedMem {
	private:
		struct ThermostatMemData {
			char TargetSensor[26];
			char LimitSensor[26];
			float TargetTemp;
			float LimitTemp;
			float Threshold;
			int32_t LastUpdated;
			Thermostat::ThermostatStatus Status;
			Thermostat::ThermostatMode Mode;
		};

		ThermostatMemData *m_data;

	public:
		ThermostatData(std::string name, bool thermOwner = false) :
			RWSharedMem(
				ThermostatData::DataPath(name),
				ThermostatData::SemPath(name),
				sizeof(ThermostatMemData),
				thermOwner) { this->m_data = (ThermostatMemData *)RWSharedMem::m_data; }

		std::string TargetSensor();
		void TargetSensor(std::string value);

		std::string LimitSensor();
		void LimitSensor(std::string value);

		float TargetTemp();
		void TargetTemp(float value);

		float LimitTemp();
		void LimitTemp(float value);

		float Threshold();
		void Threshold(float value);

		time_t LastUpdated();
		void LastUpdated(time_t value);

		Thermostat::ThermostatStatus Status();
		void Status(Thermostat::ThermostatStatus value);

		Thermostat::ThermostatMode Mode();
		void Mode(Thermostat::ThermostatMode value);

		static std::string DataPath(std::string name);
		static std::string SemPath(std::string name);
	};
} // namespace Brewserver
