#pragma once
#include <fcntl.h>
#include <semaphore.h>
#include <stddef.h>
#include <sys/mman.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <string>

namespace Brewserver {
	class SharedMemException : public std::exception {
	public:
		SharedMemException(std::string memName) :
			m_name(memName) {}
		virtual const char *what() { return (std::string("Couldn't open shared memory file: ") + this->m_name).c_str(); }

	private:
		std::string m_name;
	};

	class RWSharedMem {
	public:
		RWSharedMem(std::string name, std::string semName, size_t dataSize, bool dataOwner = false);
		~RWSharedMem();

	protected:
		size_t m_dataSize;
		void *m_data;

		void Sync();
		void Acquire();
		void Release();

	private:
		int m_fd;
		std::string m_name;
		std::string m_semName;
		sem_t *m_semaphore;
		bool m_ownData;
	};
} // namespace Brewserver
