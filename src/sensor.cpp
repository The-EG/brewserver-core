#include <chrono>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <fstream>

#include <fcntl.h>
#include <semaphore.h>

#include "sensor.hpp"

#include "mem.hpp"

using namespace std;

namespace Brewserver {
	float TemperatureSensorData::TempC()
	{
		float ret;
		this->Acquire();
		ret = this->m_data->TempC;
		this->Release();
		return ret;
	}

	void TemperatureSensorData::TempC(float value)
	{
		this->Acquire();
		this->m_data->TempC = value;
		this->Release();
	}

	float TemperatureSensorData::TempF()
	{
		return this->TempC() * (9.f / 5.f) + 32.f;
	}

	void TemperatureSensorData::TempF(float value)
	{
		this->TempC(value / (9.f / 5.f) - 32.f);
	}

	time_t TemperatureSensorData::LastUpdated()
	{
		time_t ret;
		this->Acquire();
		ret = this->m_data->LastUpdated;
		this->Release();
		return ret;
	}

	void TemperatureSensorData::LastUpdated(time_t value)
	{
		this->Acquire();
		this->m_data->LastUpdated = value;
		this->Release();
	}

	string TemperatureSensorData::DataPath(string name)
	{
		return string("/brewserver_sensor_") + name + string("_data");
	}

	string TemperatureSensorData::SemPath(string name)
	{
		return string("/brewserver_sensor_") + name;
	}

	TemperatureSensor::TemperatureSensor(
		string name,
		string id,
		long long interval,
		bool background) :
		m_interval(interval),
		m_background(background),
		m_running(false),
		m_id(id),
		m_name(name)
	{
		this->m_path = TemperatureSensor::SensorPath(this->m_id);
		this->m_data = new TemperatureSensorData(this->m_name, true);
	}

	TemperatureSensor::~TemperatureSensor()
	{
		if (this->m_running) this->Stop();

		delete this->m_data;
	}

	string TemperatureSensor::SensorPath(string id)
	{
		const string p1 = string("/sys/bus/w1/devices/");
		const string p2 = string("/w1_slave");
		return string(p1 + id + p2);
	}

	void TemperatureSensor::Update()
	{
		ifstream w1(this->m_path.c_str());

		string crc_line;
		string temp_line;

		getline(w1, crc_line);
		getline(w1, temp_line);

		w1.close();

		if (crc_line.length() < 39 ||
			temp_line.length() < 30) {
			fprintf(stderr, "Couldn't read sensor for %s, skipping update\n", this->m_name.c_str());
			return;
		}

		string crcStr = crc_line.substr(36);
		string tempStr = temp_line.substr(29);

		this->m_data->LastUpdated(time(NULL));

		if (crcStr != string("YES")) { // crc failed
			this->m_data->TempC(nanf(""));
			return;
		}

		this->m_data->TempC(stof(tempStr) / 1000.f);
	}

	float TemperatureSensor::TempC()
	{
		return this->m_data->TempC();
	}

	float TemperatureSensor::TempF()
	{
		return this->TempF();
	}

	void TemperatureSensor::Run(TemperatureSensor *sensor)
	{
		while (sensor->m_running) {
			sensor->Update();
			this_thread::sleep_for(chrono::seconds(sensor->m_interval));
		}
	}

	bool TemperatureSensor::Start()
	{
		if (!this->m_background) return false;
		this->m_running = true;
		this->m_thread = new thread(TemperatureSensor::Run, this);
		return true;
	}

	bool TemperatureSensor::Stop()
	{
		if (!this->m_background || !this->m_thread || !this->m_running)
			return false;

		this->m_running = false;
		this->m_thread->join();
		delete this->m_thread;
		this->m_thread = NULL;
		return true;
	}
} // namespace Brewserver
