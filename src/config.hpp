#include <map>
#include <string>

#include "thermostat.hpp"

namespace Brewserver {
	class Config {
	public:
		Config(std::string path);

		struct SensorConfig {
			std::string *id;
			long long interval;
		};

		struct ThermostatConfig {
			std::string *targetSensor;
			std::string *limitSensor;
			float targetTemp;
			float limitTemp;
			float threshold;
			int gpioPin;
			Thermostat::ThermostatMode mode;
		};

		std::map<std::string, SensorConfig *> sensors;
		std::map<std::string, ThermostatConfig *> thermostats;

		void parseFile(std::string path);
	};
} // namespace Brewserver
