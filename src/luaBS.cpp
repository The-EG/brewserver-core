// Lua Brewserver module

#include <lua.hpp>

#include "sensor.hpp"
#include "thermostat.hpp"

extern "C" {

using namespace Brewserver;

static int sensor_temp_f(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	float temp = -1;
	try {
		TemperatureSensorData *s = new TemperatureSensorData(name);
		temp = s->TempF();
		delete s;
	} catch (SharedMemException &ex) {
	}
	lua_pushnumber(L, temp);
	return 1;
}

static int sensor_temp_c(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	float temp = -1;
	try {
		TemperatureSensorData *s = new TemperatureSensorData(name);
		temp = s->TempC();
		delete s;
	} catch (SharedMemException &ex) {
	}
	lua_pushnumber(L, temp);
	return 1;
}

static int sensor_last_updated(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	long t = -1;
	try {
		TemperatureSensorData *s = new TemperatureSensorData(name);
		t = s->LastUpdated();
		delete s;
	} catch (SharedMemException &ex) {
	}
	lua_pushnumber(L, t);
	return 1;
}

static int thermostat_get_target_sensor(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	std::string sensor("UNK");
	try {
		ThermostatData *t = new ThermostatData(name);
		sensor = t->TargetSensor();
		delete t;
	} catch (SharedMemException &ex) {
	}
	lua_pushstring(L, sensor.c_str());
	return 1;
}

static int thermostat_get_limit_sensor(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	std::string sensor("UNK");
	try {
		ThermostatData *t = new ThermostatData(name);
		sensor = t->LimitSensor();
		delete t;
	} catch (SharedMemException &ex) {
	}
	lua_pushstring(L, sensor.c_str());
	return 1;
}

static int thermostat_get_target_temp(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	float temp = -1;
	try {
		ThermostatData *t = new ThermostatData(name);
		temp = t->TargetTemp();
		delete t;
	} catch (SharedMemException &ex) {
	}
	lua_pushnumber(L, temp);
	return 1;
}

static int thermostat_get_limit_temp(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	float temp = -1;
	try {
		ThermostatData *t = new ThermostatData(name);
		temp = t->LimitTemp();
		delete t;
	} catch (SharedMemException &ex) {
	}
	lua_pushnumber(L, temp);
	return 1;
}

static int thermostat_get_mode(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	try {
		ThermostatData *t = new ThermostatData(name);
		Thermostat::ThermostatMode m = t->Mode();

		switch (m) {
		case Thermostat::ThermostatMode::OFF:
			lua_pushstring(L, "OFF");
			break;
		case Thermostat::ThermostatMode::COOL:
			lua_pushstring(L, "COOL");
			break;
		case Thermostat::ThermostatMode::HEAT:
			lua_pushstring(L, "HEAT");
			break;
		default:
			lua_pushstring(L, "UNK");
			break;
		}
		delete t;
	} catch (SharedMemException &ex) {
		lua_pushstring(L, "UNK");
	}
	return 1;
}

static int thermostat_get_status(lua_State *L)
{
	const char *name = lua_tostring(L, 1);
	try {
		ThermostatData *t = new ThermostatData(name);
		Thermostat::ThermostatStatus s = t->Status();

		switch (s) {
		case Thermostat::ThermostatStatus::OFF:
			lua_pushstring(L, "OFF");
			break;
		case Thermostat::ThermostatStatus::ON:
			lua_pushstring(L, "ON");
			break;
		case Thermostat::ThermostatStatus::WAIT:
			lua_pushstring(L, "WAIT");
			break;
		default:
			lua_pushstring(L, "UNK");
			break;
		}
		delete t;
	} catch (SharedMemException &ex) {
		lua_pushstring(L, "UNK");
	}
	return 1;
}

static const struct luaL_Reg brewserver[] = {
	// Sensor Functions
	{ "SensorTempF", sensor_temp_f },
	{ "SensorTempC", sensor_temp_c },
	{ "SensorLastUpdated", sensor_last_updated },

	// Thermostat Functions
	{ "ThermostatGetTargetSensor", thermostat_get_target_sensor },
	{ "ThermostatGetLimitSensor", thermostat_get_limit_sensor },
	{ "ThermostatGetTargetTemp", thermostat_get_target_temp },
	{ "ThermostatGetLimitTemp", thermostat_get_limit_temp },
	{ "ThermostatGetMode", thermostat_get_mode },
	{ "ThermostatGetStatus", thermostat_get_status },

	{ NULL, NULL }
};

int luaopen_brewserver(lua_State *L)
{
	luaL_register(L, "brewserver", brewserver);
	//luaL_newlib(L, brewserver);
	return 1;
}

} // extern "C"
