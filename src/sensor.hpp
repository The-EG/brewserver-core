#pragma once

#include <cstdint>
#include <ctime>
#include <string>
#include <thread>

#include <semaphore.h>

#include "mem.hpp"

namespace Brewserver {
	class TemperatureSensorData : public RWSharedMem {
	private:
		struct SensorData {
			float TempC;
			int32_t LastUpdated;
		};

		SensorData *m_data;

	public:
		TemperatureSensorData(std::string name, bool sensorOwner = false) :
			RWSharedMem(
				TemperatureSensorData::DataPath(name),
				TemperatureSensorData::SemPath(name),
				sizeof(SensorData),
				sensorOwner) { this->m_data = (SensorData *)RWSharedMem::m_data; }

		float TempC();
		void TempC(float value);

		float TempF();
		void TempF(float value);

		time_t LastUpdated();
		void LastUpdated(time_t value);

		static std::string DataPath(std::string name);
		static std::string SemPath(std::string name);
	};

	class TemperatureSensor {
	public:
		TemperatureSensor(
			std::string name,
			std::string id,
			long long interval = 1l,
			bool background = false);
		~TemperatureSensor();

		float TempC();
		float TempF();

		bool Start();
		bool Stop();

	private:
		long long m_interval;
		bool m_background;
		bool m_running;
		std::string m_name;
		std::string m_id;
		std::string m_path;
		std::thread *m_thread;

		static std::string SensorPath(std::string id);

		void Update();

		static void Run(TemperatureSensor *sensor);
		TemperatureSensorData *m_data;
	};
} // namespace Brewserver
