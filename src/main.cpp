#include "config.hpp"
#include "sensor.hpp"
#include "sighandler.hpp"
#include "thermostat.hpp"
#include <signal.h>
#include <chrono>
#include <cstdio>
#include <list>
#include <string>
#include <thread>

int main(int argc, char *argv[])
{
	std::string confPath("/mnt/pidata/brewserver/brewserver.ini");
	Brewserver::Config *config = new Brewserver::Config(confPath);
	std::list<Brewserver::TemperatureSensor *> sensors;
	std::list<Brewserver::Thermostat *> thermostats;

	printf("Setting up sensors...\n");
	for (auto it = config->sensors.begin(); it != config->sensors.end(); ++it) {
		std::string name = it->first;
		std::string id = *(it->second->id);
		long interval = it->second->interval;
		Brewserver::TemperatureSensor *s;
		s = new Brewserver::TemperatureSensor(name, id, interval, true);
		sensors.push_back(s);
		s->Start();
	}

	printf("Waiting for sensors to take an initial reading before starting thermostats...");
	fflush(stdout);
	std::this_thread::sleep_for(std::chrono::milliseconds(1500));
	printf("done.\n");

	printf("Setting up thermostats...\n");
	fflush(stdout);
	for (auto it = config->thermostats.begin(); it != config->thermostats.end(); ++it) {
		std::string name = it->first;
		Brewserver::Config::ThermostatConfig *tc = it->second;
		Brewserver::Thermostat *t;
		t = new Brewserver::Thermostat(
			name,
			*(tc->targetSensor),
			*(tc->limitSensor),
			tc->mode,
			tc->targetTemp,
			tc->limitTemp,
			tc->threshold,
			tc->gpioPin);
		thermostats.push_back(t);
		t->Start();
	}
	fflush(stdout);

	Brewserver::SignalHandler::SetHandleSignal(SIGINT, true);
	Brewserver::SignalHandler::SetHandleSignal(SIGTERM, true);
	while (!Brewserver::SignalHandler::HasSIGINT() &&
		!Brewserver::SignalHandler::HasSIGTERM()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	printf("\nStopping thermostats...");
	fflush(stdout);
	while (!thermostats.empty()) {
		thermostats.front()->Stop();
		delete thermostats.front();
		thermostats.pop_front();
	}
	printf("done.\n");

	printf("Stopping sensors...");
	fflush(stdout);
	while (!sensors.empty()) {
		sensors.front()->Stop();
		delete sensors.front();
		sensors.pop_front();
	}
	printf("done.\n");

	return 0;
}
