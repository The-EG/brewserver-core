#include "mem.hpp"

using namespace std;

namespace Brewserver {
	RWSharedMem::RWSharedMem(string name, string semName, size_t dataSize, bool dataOwner) :
		m_name(name),
		m_semName(semName),
		m_dataSize(dataSize),
		m_ownData(dataOwner)
	{
		// setup semaphore
		this->m_semaphore = sem_open(this->m_semName.c_str(), O_CREAT, 0666, 0);

		// setup shared memory
		this->m_fd = shm_open(this->m_name.c_str(), O_RDWR | O_CREAT, 0666);
		if (this->m_fd < 0) {
			throw SharedMemException(this->m_name);
		}

		// set size and fill with null only if this is the data owner
		// otherwise it's already setup and has data
		if (this->m_ownData)
			ftruncate(this->m_fd, this->m_dataSize);

		this->m_data = mmap(
			NULL,
			this->m_dataSize,
			PROT_WRITE | PROT_READ,
			MAP_SHARED,
			this->m_fd,
			0);
		if (this->m_data == (void *)-1) {
			printf("Couldn't initialize shared memory.\n");
			abort();
		}
		this->Release(); // semaphore starts at 0
	}

	RWSharedMem::~RWSharedMem()
	{
		munmap(this->m_data, this->m_dataSize);
		close(this->m_fd);
		sem_close(this->m_semaphore);
		if (this->m_ownData) { // only unlink the files if this is the data owner
			shm_unlink(this->m_name.c_str());
			sem_unlink(this->m_semName.c_str());
		}
	}

	void RWSharedMem::Sync()
	{
		msync(this->m_data, this->m_dataSize, MS_SYNC);
	}

	void RWSharedMem::Acquire() { sem_wait(this->m_semaphore); }

	void RWSharedMem::Release() { sem_post(this->m_semaphore); }

} // namespace Brewserver
