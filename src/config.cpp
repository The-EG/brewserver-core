#include "config.hpp"
#include <cstdio>
#include <fstream>
#include <regex>

using namespace std;

namespace Brewserver {
	Config::Config(string path)
	{
		this->parseFile(path);
	}

	void Config::parseFile(string path)
	{
		string curSection("none");
		int curSectionType = 0; // 0 = skip, 1 = sensor, 2 = thermostat

		ifstream file(path.c_str());
		string line;

		regex sectionRE("\\[(.+)\\]");
		regex valueRE("([a-zA-Z_]+) ?+= ?+([a-zA-Z0-9_\\.\\-]+)");
		smatch matches;

		while (getline(file, line)) {
			if (line.length() == 0) continue;
			if (regex_search(line, matches, sectionRE)) {
				string section = matches[1];
				if (regex_search(section, matches, valueRE)) {
					string type = matches[1];
					string name = matches[2];

					curSection = name;
					if (type == "Sensor") {
						curSectionType = 1;
						this->sensors.insert(
							pair<string, SensorConfig *>(
								curSection,
								new SensorConfig()));
					} else if (type == "Thermostat") {
						curSectionType = 2;
						this->thermostats.insert(
							pair<string, ThermostatConfig *>(
								curSection,
								new ThermostatConfig()));
					} else {
						curSectionType = 0;
						fprintf(stderr,
							"%s not a recognized section type; "
							"must be one of: Sensor, Thermostat. Skipping.\n",
							type.c_str());
					}
				} else {
					fprintf(stderr,
						"Invalid section format (%s), must be Type=Name\n",
						section.c_str());
					curSectionType = 0;
				}
			} else if (regex_search(line, matches, valueRE)) {
				if (curSectionType == 0) continue;
				string key = matches[1];
				string value = matches[2];

				switch (curSectionType) {
				case 1: // sensor
					if (key == "id") {
						this->sensors[curSection]->id = new string(value);
					} else if (key == "interval") {
						this->sensors[curSection]->interval = stol(value);
					} else {
						fprintf(stderr, "%s is not a valid option for sensors.\n", key.c_str());
					}
					break;
				case 2: // thermostat
					if (key == "mode") {
						if (value == "cool")
							this->thermostats[curSection]->mode = Thermostat::ThermostatMode::COOL;
						else if (value == "heat")
							this->thermostats[curSection]->mode = Thermostat::ThermostatMode::HEAT;
						else
							fprintf(stderr,
								"%s not a valid thermostat mode ('cool', 'heat')\n",
								value.c_str());
						break;
					} else if (key == "targetSensor")
						this->thermostats[curSection]->targetSensor = new string(value);
					else if (key == "limitSensor")
						this->thermostats[curSection]->limitSensor = new string(value);
					else if (key == "targetTemp")
						this->thermostats[curSection]->targetTemp = stof(value);
					else if (key == "limitTemp")
						this->thermostats[curSection]->limitTemp = stof(value);
					else if (key == "gpiopin")
						this->thermostats[curSection]->gpioPin = stoi(value);
					else if (key == "threshold")
						this->thermostats[curSection]->threshold = stof(value);
					break;
				}
			}
		}
	}
} // namespace Brewserver
