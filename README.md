# The-EG's Brewserver
The Brewserver is a Raspberry Pi 3B that monitors and controls a fermentation environment (fancy term for a chest freezer) for fermenting home brewed beer.

## Brewserver-Core

This repository houses the code for the core Brewserver library and service control:
 - Temperature Sensors
 - Relay Control (for switching AC)
 - Thermostats
 - Shared Memory for monitoring and control
 - A Lua interface to the shared memory (optional)

## Compiling

Build with CMake:
 - Make a build directory: `mkdir build`
 - Enter it: `cd build`
 - Configure CMake:
   - `cmake ..`  
     or
   - `../scripts/conf_debug_clang.sh` (requires clang7)  
     or
   - `../scripts/conf_release_clang.sh` (requires clang7)
 - Build: `make`

The current configuration assumes both the shared library and executable are in the root directory:

~~~
ln -s build/libbrewserver.so .
ln -s build/brewserver-ctl .
~~~

Or, move them to an appropriate location.

## Service installation

 - Edit the `ExecStart` value of services/brewserver-ctl.service to the appropriate path of the brewserver-ctl executable
 - Copy brewserver-ctl.service to the systemd services folder: `sudo cp services/brewserver-ctl.service /etc/systemd/system/`
 - Tell systemd to reload unit files: `sudo systemctl daemon-reload`
 - Start the service: `sudo systemctl start brewserver-ctl`
 - Optionally, check that it worked: `sudo systemctl status brewserver-ctl`
